AFRAME.registerComponent('menu-page', {
    schema: {},
    init: function() {
        var el = this.el;
        var sceneEl = document.querySelector('a-scene')
        var viveHotspotEl = document.querySelector('#vive-hotspot');

        var loadScene = function(href) {
            var html = loadPage(href);
            sceneEl.insertAdjacentHTML('afterbegin', html);
        }

        var loadPage = function(href) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", href, false);
            xmlhttp.send();
            return xmlhttp.responseText;
        }

        var clearScene = function() {
            var childEntity = sceneEl.querySelector('#menu-page');
            if (childEntity) {
                sceneEl.removeChild(childEntity);
            }
        }

        var clearAssets = function() {
            var assetEntitys = sceneEl.querySelectorAll('a-assets');
            if (assetEntitys.length === 2) {
                sceneEl.removeChild(assetEntitys[1]);
            }
        }

        viveHotspotEl.addEventListener('click', function() {
        	viveHotspotEl.setAttribute('visible', 'false');
            loadScene('./template/vr_product.html');
            setTimeout(function() {
                clearScene();
                clearAssets();
            }, 1000);
        });
    },
    update: function() {}
});

AFRAME.registerComponent('product-component', {

        init: function() {
            var el = this.el;
            var sceneEl = document.querySelector('a-scene');
            var skyEl = document.querySelector('a-sky');
            var cameraEl = document.querySelector('#camera');
            var cursorEl = document.querySelector('#cursor');
            var objEl = el.querySelector('a-obj-model');
            var imageEls = el.querySelectorAll('a-image');
            var controllersEl = el.querySelector('a-entity');
            var allcontrolers = controllersEl.querySelectorAll('a-image');
            // object rotation animation
            var rotateLeftInvl;
            // used to store camera rotate angle
            var cameraAngle;
            // vr Mode token
            var isVRMode = false;

            var isIntersected = false;

            // enter vr mode
            sceneEl.addEventListener('enter-vr', function() {
                isVRMode = true;
                // if (AFRAME.utils.device.isMobile()) {
                cursorEl.setAttribute('visible', 'true');
                cursorEl.setAttribute('cursor', {
                    fuse: true,
                    fuseTimeout: 1000
                });
                // }
            });

            // exit vr mode
            sceneEl.addEventListener('exit-vr', function() {
                isVRMode = false;
                cursorEl.setAttribute('visible', 'false');
                cursorEl.setAttribute('cursor', {
                    fuse: false,
                    fuseTimeout: 1000
                });
            });

            // mouse enter product, show loading cursor
            imageEls[0].addEventListener('raycaster-intersected', function() {
                if (isVRMode && !isIntersected) {
                    cursorEl.emit('cursor-intersect');
                    isIntersected = true;
                }
            });

            imageEls[0].addEventListener('raycaster-intersected-cleared', function() {
                isIntersected = false;
                if (isVRMode) {
                    cursorEl.emit('cursor-clear');
                    cursorEl.setAttribute('radius-inner', '0.05');
                }
            });

            // show product info
            imageEls[0].addEventListener('click', function() {
                // show product info
                imageEls[0].setAttribute('visible', 'false');
                objEl.setAttribute('visible', 'true');
                controllersEl.setAttribute('visible', 'true');
                for (var i = 1, len = imageEls.length; i < len; i++) {
                    imageEls[i].setAttribute('visible', 'true');
                }
                // opacity background
                skyEl.setAttribute('opacity', '0.5');
                // objel start animation
                objEl.emit('start-ani-event', {}, false);
                // rotate model
                rotateModel(0, -2);

                // set up look controls, only for pc
                if (!isVRMode) {
                    // store rotation angle
                    cameraAngle = cameraEl.getAttribute('rotation');
                    cameraEl.setAttribute('look-controls', 'enabled: false');
                    objEl.setAttribute('look-controls', 'enabled: true');
                }
            });

            // close product info
            imageEls[1].addEventListener('click', function() {
                // hide product info
                imageEls[0].setAttribute('visible', 'true');
                objEl.setAttribute('visible', 'false');
                controllersEl.setAttribute('visible', 'false');
                for (var i = 1, len = imageEls.length; i < len; i++) {
                    imageEls[i].setAttribute('visible', 'false');
                }
                // opacity background
                skyEl.setAttribute('opacity', '1');
                // stop rotating model
                clearInterval(rotateLeftInvl);
                // set up look cont
                if (!isVRMode) {
                    cameraEl.setAttribute('look-controls', 'enabled: true');
                    objEl.setAttribute('look-controls', 'enabled: false');
                    // restore camera rotation
                    cameraEl.setAttribute('rotation', cameraAngle);
                }
            });

            imageEls[1].addEventListener('raycaster-intersected', function() {
                if (isVRMode && !isIntersected) {
                    imageEls[1].setAttribute('src', '#close_hover');
                    cursorEl.emit('cursor-intersect');
                    isIntersected = true;
                }
            });

            imageEls[1].addEventListener('raycaster-intersected-cleared', function() {
                isIntersected = false;
                if (isVRMode) {
                    imageEls[1].setAttribute('src', '#close');
                    cursorEl.emit('cursor-clear');
                    cursorEl.setAttribute('radius-inner', '0.05');
                }
            });

            // left rotate obj
            allcontrolers[0].addEventListener('click', function() {
                rotateModel(0, -1);
            });

            allcontrolers[0].addEventListener('raycaster-intersected', function() {
                if (isVRMode && !isIntersected) {
                    allcontrolers[0].setAttribute('src', '#left_hover');
                    cursorEl.emit('cursor-intersect');
                    isIntersected = true;
                }
            });

            allcontrolers[0].addEventListener('raycaster-intersected-cleared', function() {
                isIntersected = false;
                if (isVRMode) {
                    allcontrolers[0].setAttribute('src', '#left');
                    cursorEl.emit('cursor-clear');
                    cursorEl.setAttribute('radius-inner', '0.05');
                }
            });

            // up rotate obj
            allcontrolers[1].addEventListener('click', function() {
                rotateModel(-1, 0);
            });

            allcontrolers[1].addEventListener('raycaster-intersected', function() {
                if (isVRMode && !isIntersected) {
                    allcontrolers[1].setAttribute('src', '#up_hover');
                    cursorEl.emit('cursor-intersect');
                    isIntersected = true;
                }
            });

            allcontrolers[1].addEventListener('raycaster-intersected-cleared', function() {
                isIntersected = false;
                if (isVRMode) {
                    allcontrolers[1].setAttribute('src', '#up');
                    cursorEl.emit('cursor-clear');
                    cursorEl.setAttribute('radius-inner', '0.05');
                }
            });

            // down rotate obj
            allcontrolers[2].addEventListener('click', function() {
                rotateModel(1, 0);
            });

            allcontrolers[2].addEventListener('raycaster-intersected', function() {
                if (isVRMode && !isIntersected) {
                    allcontrolers[2].setAttribute('src', '#down_hover');
                    cursorEl.emit('cursor-intersect');
                    isIntersected = true;
                }
            });

            allcontrolers[2].addEventListener('raycaster-intersected-cleared', function() {
                isIntersected = false;
                if (isVRMode) {
                    allcontrolers[2].setAttribute('src', '#down');
                    cursorEl.emit('cursor-clear');
                    cursorEl.setAttribute('radius-inner', '0.05');
                }
            });

            // right rotate obj
            allcontrolers[3].addEventListener('click', function() {
                rotateModel(0, 1);
            });

            allcontrolers[3].addEventListener('raycaster-intersected', function() {
                if (isVRMode && !isIntersected) {
                    allcontrolers[3].setAttribute('src', '#right_hover');
                    cursorEl.emit('cursor-intersect');
                    isIntersected = true;
                }
            });

            allcontrolers[3].addEventListener('raycaster-intersected-cleared', function() {
                isIntersected = false;
                if (isVRMode) {
                    allcontrolers[3].setAttribute('src', '#right');
                    cursorEl.emit('cursor-clear');
                    cursorEl.setAttribute('radius-inner', '0.05');
                }
            });
            // using interval to set rotation params
            var rotateModel = function(x, y) {
                clearInterval(rotateLeftInvl);
                rotateLeftInvl = setInterval(function() {
                    var rotationParams = objEl.getAttribute('rotation');
                    objEl.setAttribute('rotation', {
                        x: rotationParams.x + x,
                        y: rotationParams.y + y,
                        z: rotationParams.z
                    });
                }, 100);
            }
        }
    });
AFRAME.registerComponent('global-scene', {
        schema: {},
        init: function() {
            var el = this.el;
            var homePage = './template/vr_product.html';

            var loadScene = function(href) {
                var html = loadPage(href);
                el.insertAdjacentHTML('afterbegin', html);
            }

            var loadPage = function(href) {
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET", href, false);
                xmlhttp.send();
                return xmlhttp.responseText;
            }

            var clearScene = function() {
                var childEntity = el.querySelector('a-entity');
                if (childEntity) {
                    el.removeChild(childEntity);
                }
            }

            var clearAssets = function() {
                var assetEntity = el.querySelector('a-assets');
                if (assetEntity) {
                    el.removeChild(assetEntity);
                }
            }

            loadScene(homePage);

            // el.addEventListener('click', function() {
            //     clearScene();
            //     clearAssets();
            //     loadScene('./template/home.html');
            // });
        },
        update: function() {}
    });
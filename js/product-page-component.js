AFRAME.registerComponent('product-page', {
    schema: {},
    init: function() {
        var el = this.el;
        var sceneEl = document.querySelector('a-scene')
        var itHotspotEl = document.querySelector('#it-hotspot');

        var loadScene = function(href) {
            var html = loadPage(href);
            sceneEl.insertAdjacentHTML('afterbegin', html);
        }

        var loadPage = function(href) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", href, false);
            xmlhttp.send();
            return xmlhttp.responseText;
        }

        var clearScene = function() {
            var childEntity = sceneEl.querySelector('#product-page');
            if (childEntity) {
                sceneEl.removeChild(childEntity);
            }
        }

        var clearAssets = function() {
            var assetEntitys = sceneEl.querySelectorAll('a-assets');
            if (assetEntitys.length === 2) {
                sceneEl.removeChild(assetEntitys[1]);
            }
        }

        itHotspotEl.addEventListener('click', function() {
        	itHotspotEl.setAttribute('visible', 'false');
            loadScene('./template/vr_menu.html');
            setTimeout(function() {
                clearScene();
                clearAssets();
            }, 1000);
        });
    },
    update: function() {}
});
